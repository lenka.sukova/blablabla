/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controler;

import Model.TelSeznamException;
import java.io.IOException;
import java.lang.reflect.Executable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sukovi
 */
public class TelSeznamControllerTest {
    TelSeznamController instance;
    
    public TelSeznamControllerTest() {
        try {
            instance = new TelSeznamController();
        } catch (IOException ex) {
            Logger.getLogger(TelSeznamControllerTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TelSeznamControllerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        instance = null;
    }

    /*@Test
    public void testGetZahlavi() {
        System.out.println("getZahlavi");
        TelSeznamController instance = new TelSeznamController();
        String[] expResult = null;
        String[] result = instance.getZahlavi();
        assertArrayEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetData_0args() {
        System.out.println("getData");
        TelSeznamController instance = new TelSeznamController();
        ArrayList expResult = null;
        ArrayList result = instance.getData();
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetData_ArrayList() {
        System.out.println("getData");
        ArrayList<Object> params = null;
        TelSeznamController instance = new TelSeznamController();
        ArrayList expResult = null;
        ArrayList result = instance.getData(params);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetData_3args() {
        System.out.println("getData");
        ArrayList<Object> params = null;
        boolean useCaseSensitive = false;
        boolean useDiacritics = false;
        TelSeznamController instance = new TelSeznamController();
        ArrayList expResult = null;
        ArrayList result = instance.getData(params, useCaseSensitive, useDiacritics);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    @Test
    public void testImportCSV() {
        System.out.println("importCSV");
        String impSoubor = "";
        TelSeznamController instance = new TelSeznamController();
        instance.importCSV(impSoubor);
        fail("The test case is a prototype.");
    }

    @Test
    public void testExportCSV() {
        System.out.println("exportCSV");
        String expSoubor = "";
        TelSeznamController instance = new TelSeznamController();
        instance.exportCSV(expSoubor);
        fail("The test case is a prototype.");
    }

    @Test
    public void testVypisTelSeznam() {
        System.out.println("vypisTelSeznam");
        TelSeznamController instance = new TelSeznamController();
        instance.vypisTelSeznam();
        fail("The test case is a prototype.");
    }

    @Test
    public void testSerialize() throws Exception {
        System.out.println("Serialize");
        TelSeznamController instance = new TelSeznamController();
        instance.Serialize();
        fail("The test case is a prototype.");
    }

    @Test
    public void testFoundAllByName() {
        System.out.println("foundAllByName");
        String name = "";
        boolean useCaseSensitive = false;
        boolean useDiacritics = false;
        TelSeznamController instance = new TelSeznamController();
        ArrayList expResult = null;
        ArrayList result = instance.foundAllByName(name, useCaseSensitive, useDiacritics);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }
    */
    @Test
    public void testAddPolozka() {
        //polozka, kterou vlozime    
        String[] polozkaVlozena = {"test1ADDJmeno", "testFunkce", "testPopisCinnosti","testCisloDveri","testTelefon","testMobil","testEmail","testOddeleni","testZkratkaOdboru","NazevOdboru"};
        int polozkaHashCode = polozkaVlozena.hashCode();
        
        //hledame jeji hashcode pred vlozenim
        String[] polozkaNalezena = instance.getPolozkaByHashCode(polozkaHashCode);
        assertNull(polozkaNalezena);
        
        //polozku vlozime
        instance.addPolozka(polozkaVlozena);
        
        //hledame jeji hashcode po vlozeni
        polozkaNalezena = instance.getPolozkaByHashCode(polozkaHashCode);
        int polozkaNalezenaHashCode = polozkaNalezena.hashCode();
        assertEquals(polozkaHashCode, polozkaNalezenaHashCode);
        assertArrayEquals(polozkaVlozena, polozkaNalezena);   

    }
    
    /*@Test
    public void testAddPolozka_duplicitniKlic() {
        
        //polozka, kterou vlozime    
        String[] polozkaVlozena = {"test1ADDJmeno", "testFunkce", "testPopisCinnosti","testCisloDveri","testTelefon","testMobil","testEmail","testOddeleni","testZkratkaOdboru","NazevOdboru"};
        int polozkaHashCode = polozkaVlozena.hashCode();
        
        //hledame jeji hashcode pred vlozenim
        String[] polozkaNalezena = instance.getPolozkaByHashCode(polozkaHashCode);
        assertNull(polozkaNalezena);
        
        //polozku vlozime
        instance.addPolozka(polozkaVlozena);
        polozkaNalezena = instance.getPolozkaByHashCode(polozkaHashCode);
        int polozkaNalezenaHashCode = polozkaNalezena.hashCode();
        assertEquals(polozkaHashCode, polozkaNalezenaHashCode);
        
        //polozku vlozime znovu
        instance.addPolozka(polozkaVlozena);  
        
        assertThrows(TelSeznamException.class, () -> {
            instance.addPolozka(polozkaVlozena); 
        });

    }    */

    @Test
    public void testRemovePolozka() {
        //polozka, kterou vlozime    
        String[] polozkaVlozena = {"testREMOVEJmeno1", "testFunkce", "testPopisCinnosti","testCisloDveri","testTelefon","testMobil","testEmail","testOddeleni","testZkratkaOdboru","NazevOdboru"};
        int polozkaHashCode = polozkaVlozena.hashCode();
        
        //polozku vlozime
        instance.addPolozka(polozkaVlozena);
        
        //hledame jeji hashcode po vlozeni
        String[] polozkaNalezena = instance.getPolozkaByHashCode(polozkaHashCode);
        int polozkaNalezenaHashCode = polozkaNalezena.hashCode();
        assertEquals(polozkaHashCode, polozkaNalezenaHashCode);
        assertArrayEquals(polozkaVlozena, polozkaNalezena); 
        
        //polozku vymazeme
        instance.removePolozka(polozkaNalezena);
        
        //hledame jeji hashcode po vymazani
        String[] polozkaVymazana = instance.getPolozkaByHashCode(polozkaHashCode);
        assertNull(polozkaVymazana);      
    }

    @Test
    public void testEditPolozka() {
        //polozka, kterou vlozime    
        String[] polozkaVlozena = {"testEDITJmeno1", "testFunkce", "testPopisCinnosti","testCisloDveri","testTelefon","testMobil","testEmail","testOddeleni","testZkratkaOdboru","NazevOdboru"};
        int polozkaHashCode = polozkaVlozena.hashCode();
        instance.addPolozka(polozkaVlozena);
        String[] polozkaNalezena = instance.getPolozkaByHashCode(polozkaHashCode);
        assertArrayEquals(polozkaVlozena, polozkaNalezena);
        
        //polozka se zmenenymi hodnotami    
        String[] polozkaZmenena = {"testJmeno123", "testFunkce", "testPopisCinnosti","testCisloDveri","testTelefon","testMobil","testEmail","testOddeleni","testZkratkaOdboru","NazevOdboru"};
        int polozkaZmenenaHashCode = polozkaZmenena.hashCode();

        //zmen polozku
        instance.editPolozka(polozkaVlozena, polozkaZmenena);
        
        //puvodni polozka byla odstranena
        polozkaNalezena = instance.getPolozkaByHashCode(polozkaHashCode);
        assertNull(polozkaNalezena);
        
        //nova polozka byla vlozena
        polozkaNalezena = instance.getPolozkaByHashCode(polozkaZmenenaHashCode);
        assertArrayEquals(polozkaZmenena, polozkaNalezena);
    }

}
