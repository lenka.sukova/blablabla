/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

//import static com.ibm.xtq.xslt.typechecker.v1.types.Type.Int;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.PatternSyntaxException;
import javax.swing.JOptionPane;

/**
 *
 * @author sukovi
 */
public class TelSeznam extends HashMap<Integer,String[]> implements Serializable {
    /** nazev importovaneho csv souboru */
    private final String IMP_SOUBOR = "tel_seznam_KUp_2.csv";
    /** nazev exportního csv souboru */
    private final String EXP_SOUBOR = "EXP_tel_seznam_KUp_2.csv";
    /** nazev serializovaného souboru */
    private final String SOUBOR = "TelSeznam.dat";    
    /** zahlavi souboru - struktura vety */
    private String[] zahlavi;

    
    public TelSeznam() {
        this.importCSV(IMP_SOUBOR);
    }

    @Override
    public String toString() {
        return "telSeznam{" + "soubor=" + IMP_SOUBOR + '}';
    }

    /**
     * @return pole s položkami záhlaví 
     */
    public String[] getZahlavi() {
        return zahlavi;
    }

    /**
     * Nastaví záhlaví podle vstupního parametru
     * @param zahlavi pole s položkami záhlaví
     */
    public void setZahlavi(String[] zahlavi) {
        this.zahlavi = zahlavi;
    }
    
    /**
     * Provede import položek ze zadaného csv souboru
     * @param impSoubor jméno souboru
     */
    public void importCSV(String impSoubor){
        
        try (BufferedReader br = new BufferedReader(new FileReader(impSoubor)))
        {
            String radek;
            while ((radek = br.readLine()) != null)
            {
                String[] hodnotyRadku = radek.split(";");
                //1. radek bude zahlavi
                if (this.zahlavi == null){
                   zahlavi = hodnotyRadku; 
                }               
                //ostatni radky dej do kolekce
                else{
                    this.put(hodnotyRadku.hashCode(),hodnotyRadku);
                }
            }
        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
        }      
    }
    
    /**
     * Vypíše seznam na obrazovku
     * pro lepší čitelnost bez diakritiky
     */
    public void vypisTelSeznam(){
        String result = String.join(";",this.zahlavi) + "\n";
        for(int i=0; i<this.size() ; i++){
           result += odstranDiakritiku(String.join(";", this.get(i))) + "\n";
        } 
        result += "*************************************************************\n";
        result += "Pocet polozek: " + this.getPocetPolozek();
        System.out.println(result);
    }
    
    /**
     * @return počet položek, které jsou právě naimportovány 
     */
    public int getPocetPolozek(){
        return this.size();
    }
    
    /*
     * Odstraní diakritiku ze zadaného řetězce
     */
    private String odstranDiakritiku(String s){
        String result = s;
        String[][] regPole = {{"A", "[ÁÄ]"},
                              {"E","[ÉĚ]"},
                              {"I","[Í]"},
                              {"O","[ÓÖ]"},
                              {"U","[ÚŮÜ]"},
                              {"Y","[Ý]"},
                              {"Z","[Ž]"},
                              {"S","[Š]"},
                              {"C","[Č]"},
                              {"R","[Ř]"},
                              {"D","[Ď]"},
                              {"T","[Ť]"},
                              {"N","[Ň]"},
                              {"a", "[áä]"},
                              {"e","[éě]"},
                              {"i","[í]"},
                              {"o","[óö]"},
                              {"u","[ůúü]"},
                              {"y","[ý]"},
                              {"z","[ž]"},
                              {"s","[š]"},
                              {"c","[č]"},
                              {"r","[ř]"},
                              {"d","[ď]"},
                              {"t","[ť]"},
                              {"n","[ň]"}};
        
        for(int i=0; i<regPole.length; i++){
            result = result.replaceAll(regPole[i][1], regPole[i][0]);
        }
        
        return result;
    }
    
    /**
     * Provede export do zadaného souboru
     * @param expSoubor jméno souboru
     */
    public void exportCSV(String expSoubor){
        //uloz zahlavi
        this.ulozRadek(expSoubor, String.join(";",this.zahlavi));
        //uloz data
        for(int i=0; i<this.size() ; i++){
            this.ulozRadek(expSoubor, String.join(";", (new ArrayList<String[]> (this.values())).get(i)));
        }   
    }
    
    /*
     * Uloží řádek do souboru 
     */
    private void ulozRadek(String expSoubor, String radek){
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(expSoubor, true))){
            
            bw.write(radek);
            bw.newLine();
            bw.flush();

        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }        
    }              
    
    /**
     * Ukládá data do serializovaného souboru
     * 
     * @throws FileNotFoundException ..
     * @throws IOException ..
     * @throws NotSerializableException .. 
     */
    public void Serialize() throws FileNotFoundException, IOException, NotSerializableException{        
        FileOutputStream fos = new FileOutputStream(SOUBOR);       
        ObjectOutputStream objOut = new ObjectOutputStream(fos); 
        objOut.writeObject(this);
  
        objOut.close();
        fos.close(); 
    }
    
    /**
     * Vrátí data seznamu
     * @return seznam položek a jejich hodnot
     */
    public ArrayList<String[]> getData(){
        return new ArrayList<String[]>(this.values());
    }
    
    /**
     * Vrátí data seznamu podle vstupních parametrů
     * @param params parametry
     * @return seznam položek a jejich hodnot 
     */
  /*  public ArrayList<String[]> getData(ArrayList<Object> params){
        return getData(params, true, true);
    } */   
    
    /**
     * 
     * @param params parametry
     * @param useCaseSensitive true - při vyhledávání se zohledňují malá a velká písmena
     * @param useDiacritics true - při vyhledávání se zohledňuje diakritika
     * @return seznam položek a jejich hodnot 
     */
    public ArrayList<String[]> getData(ArrayList<String> params, boolean useCaseSensitive, boolean useDiacritics){

        if(params == null || params.size()==0) return this.getData();
        
        ArrayList<String[]> result = new ArrayList();
        for(String[] radek : this.getData()){

            if(isShoda(radek[0], (String) params.get(0), useCaseSensitive, useDiacritics) &&
               isShoda(radek[1], (String) params.get(1), useCaseSensitive, useDiacritics) &&
               isShoda(radek[2], (String) params.get(2), useCaseSensitive, useDiacritics) &&
               isShoda(radek[3], (String) params.get(3), useCaseSensitive, useDiacritics) &&
               isShoda(radek[4], (String) params.get(4), useCaseSensitive, useDiacritics) &&
               isShoda(radek[5], (String) params.get(5), useCaseSensitive, useDiacritics) &&
               isShoda(radek[6], (String) params.get(6), useCaseSensitive, useDiacritics) &&
               isShoda(radek[7], (String) params.get(7), useCaseSensitive, useDiacritics) &&
               isShoda(radek[8], (String) params.get(8), useCaseSensitive, useDiacritics) &&
               isShoda(radek[9], (String) params.get(9), useCaseSensitive, useDiacritics)){                    

                result.add(radek);
                
            } 
        }
        /*for(String[] radek : this.getData()){
           
           if(isShoda(radek[0],  params.get(0), useCaseSensitive, useDiacritics) ||  //jmeno
               isShoda(radek[1],  params.get(1), useCaseSensitive, useDiacritics) ||    //funkce  
               isShoda(radek[4],  params.get(2), useCaseSensitive, useDiacritics) ||   // telefon
               isShoda(radek[5],  params.get(3), useCaseSensitive, useCaseSensitive) ||  // mobil 
               isShoda(radek[6],  params.get(4), useCaseSensitive, useCaseSensitive) ||   // mail
               isShoda(radek[8],  params.get(5), false, false) ){    //obor              

                result.add(radek);
              //   System.out.println(radek[0]+" --"+radek[1]+"-- "+radek[2]+"-- "+radek[3]+" --"+radek[4]+"-- "+radek[5]);
            } 
        }*/
        return result;
    }
      /**
     * Vrátí  data ze seznamu podle jmena nebo casti 
     * @param name jmeno nebo casti jmena
     * @return seznam položek a jejich hodnot
     */
    public ArrayList<String[]> foundAllByName(String name, boolean useCaseSensitive, boolean useDiacritics){
        ArrayList<String[]> results= new ArrayList<String[]>();
         for(String[] radek : this.getData()){
            if(isShoda(radek[0], name, useCaseSensitive, useDiacritics) ){                    

                results.add(radek);
            } 
        }
       
        return results;
    }
    
    /*
     * Vrátí, zda řádek vyhovuje vyhledávacím kritériím a bude zařazen do výstupu
    */
    private boolean isShoda (String hodnota, String param, boolean useCaseSensitive, boolean useDiacritics){     
        boolean result = false;
        if (param == null || param.matches("^[\\s]*$")){
           result = true;
        }else{

        String param_ = useCaseSensitive ? param : param.toUpperCase();
        param_ = useDiacritics ? param_ : this.odstranDiakritiku(param_);
        String hodnota_ = useCaseSensitive ? hodnota : hodnota.toUpperCase();
        hodnota_ = useDiacritics ? hodnota_ : this.odstranDiakritiku(hodnota_);
        
        try{
            result = hodnota_.matches(param_.replace("%", ".*"));        
        }
        catch(PatternSyntaxException ex){
            result = false;
        }
        
       }
        return result;
    }    
    
    public void addPolozka(String[] polozka){
        this.put(polozka.hashCode(), polozka);
    }
    
    public void removePolozka(String[] polozka){
        this.remove(polozka.hashCode());
    }
    
    public String[] getPolozkaByHashCode(int hashCode){
        return this.get(hashCode);
    }    

}
