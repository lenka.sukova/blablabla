/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controler;

import Model.TelSeznam;
import blablabla.blablabla;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sukovi
 */
public class TelSeznamController {
    TelSeznam telSeznam;
    
    public TelSeznamController() throws IOException, ClassNotFoundException {
      //  this.telSeznam = this.telSeznamInit();
       this.telSeznam = new TelSeznam(); 
      //  this.telSeznam.Serialize();
    }
    
    /*
     * Pokud existuje serializovaný soubor, načte ho do objektu
     * Pokud soubor neexistuje, vytvoří prázdný objekt
     */
    private TelSeznam telSeznamInit () throws IOException, ClassNotFoundException{
        final String SOUBOR = "TelSeznam.dat";
        TelSeznam result = null;
        
        try{
            result = DeSerializeTelSeznam(SOUBOR);
        }
        catch(FileNotFoundException ex){
            result = new TelSeznam();
        }
        return result;
    }
    
    /*
     * Načítá data ze serializovaného souboru
     */
    private TelSeznam DeSerializeTelSeznam(String soubor) throws FileNotFoundException, IOException, ClassNotFoundException {
        TelSeznam result = null;

        FileInputStream fis = new FileInputStream(soubor);
        ObjectInputStream objIn = new ObjectInputStream(fis);
        Object object = objIn.readObject();   

        if(object != null || object instanceof TelSeznam){
            result = (TelSeznam) object;
        }
        else{
            result = new TelSeznam();
        }

        objIn.close();
        fis.close();  
        
        return result;        
    }     
    
    /**
     * Vrátí názvy jednotlivých položek seznamu
     * @return pole řetězců
     */
    public String[] getZahlavi() {
        return this.telSeznam.getZahlavi();
    }
    
    /**
     * Vrátí data
     * @return seznam dat vyhovujících kritériím
     */
    public ArrayList<String[]> getData(){
        return this.telSeznam.getData();
    }

    /**
     * Vrátí data na základě zadaných parametrů
     * params seznam 10 vyhledávacích kritérií
     * @return seznam dat vyhovujících kritériím 
     */    
  /*  public ArrayList<String[]> getData(ArrayList<Object> params){
        return this.telSeznam.getData(params);
    }  */  

    /**
     * Vrátí data na základě zadaných parametrů
     * params seznam 10 vyhledávacích kritérií
     * useCaseSensitive true výběr bude case sensitive
     * useDiacritics true bude se zohledňovat diakritika
     * @return seznam dat vyhovujících kritériím 
     */    
    public ArrayList<String[]> getData(ArrayList<String> params, boolean useCaseSensitive, boolean useDiacritics){
        return this.telSeznam.getData(params, useCaseSensitive, useDiacritics);
    }  
    
    public String[] getPolozkaByHashCode(int hashCode){
        return this.telSeznam.getPolozkaByHashCode(hashCode);
    }
    
    /**
     * Provede import ze zadaného csv souboru
     * @param impSoubor jmeno souboru
     */
    public void importCSV(String impSoubor){
        this.telSeznam.importCSV(impSoubor);
    }
    
    /**
     * Provede export do zadaného csv souboru
     * @param expSoubor jméno souboru
     */
    public void exportCSV(String expSoubor){
        this.telSeznam.exportCSV(expSoubor);
    }

    /**
     * Vypíše obsah seznamu na obrazovku
     */
    public void vypisTelSeznam(){
        this.telSeznam.vypisTelSeznam();
    }    
    
    /**
     * Ukládá data do serializovaného souboru
     * 
     * @throws FileNotFoundException ..
     * @throws IOException ..
     * @throws NotSerializableException .. 
     */
    public void Serialize() throws FileNotFoundException, IOException, NotSerializableException{  
        this.telSeznam.Serialize();
    } 
      /**
     * Vrátí  data ze seznamu podle jmena nebo casti 
     * @param name jmeno nebo cast jmena
     * @return seznam položek a jejich hodnot
     */
     public ArrayList<String[]> foundAllByName(String name, boolean useCaseSensitive, boolean useDiacritics){
      
        return this.telSeznam.foundAllByName(name, useCaseSensitive,  useDiacritics);
    }       
    
    /**
     * Vlozi novou polozku do seznamu
     * @param polozka pole s hodnotami polozky
     */
    public void addPolozka(String[] polozka){
        this.telSeznam.addPolozka(polozka);
    }
    
    /**
     * Odstraní položku ze seznamu
     * @param polozka pole s hodnotami polozky 
     */
    public void removePolozka(String[] polozka){
        this.telSeznam.removePolozka(polozka);
    }
    
    /**
     * Změní položku v seznamu
     * @param polozkaRemoved původní položka
     * @param polozkaAdd nová položka
     */
    public void editPolozka(String[] polozkaRemoved, String[] polozkaAdd){
        this.telSeznam.removePolozka(polozkaRemoved);
        this.telSeznam.addPolozka(polozkaAdd);
    }
}
